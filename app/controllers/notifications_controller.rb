class NotificationsController < ApplicationController

  def index
    @notifications = Notification.all.about_me(current_user.id)
    update_notifications
  end

  private
    def update_notifications
      @notifications.update_all(checked: true)
    end
end
