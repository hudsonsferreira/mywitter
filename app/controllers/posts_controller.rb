class PostsController < ApplicationController

  def index
    @posts = Post.all.includes_reversed_user
  end

  def new
    @posts = index
    @post  = current_user.posts.build if user_signed_in?
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to request.referer
    else
      render :new
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  private
    def post_params
      params.require(:post).permit(:content)
    end

end
