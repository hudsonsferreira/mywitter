class RelationshipsController < ApplicationController

  def create
    @relationship = current_user.relationships.create(followed_id: params[:followed_id])
    @relationship.sends_user_follow_notification
    redirect_to request.referer
  end

  def destroy
    @relationship = current_user.relationships.find(params[:id])
    @relationship.sends_user_unfollow_notification
    @relationship.destroy
    redirect_to request.referer
  end

end
