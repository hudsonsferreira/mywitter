class Notification < ActiveRecord::Base
  belongs_to :user
  belongs_to :following, class_name: 'User', foreign_key: 'followed_id'

  validates :content, :followed_id, presence: true

  scope :includes_reversed_following, -> { includes(:following).reverse }

  def self.about_me(current_user_id)
    where(followed_id: current_user_id).includes(:following).reverse_order
  end

  def self.notifications_not_seen_counter(current_user_id)
    Notification.all.about_me(current_user_id).where(checked: false).count
  end
end
