class Post < ActiveRecord::Base
  belongs_to :user
  validates :content, presence: true
  scope :includes_reversed_user, -> { includes(:user).reverse }
end
