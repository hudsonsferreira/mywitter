class Relationship < ActiveRecord::Base
  belongs_to :user
  belongs_to :following, class_name: 'User', foreign_key: 'followed_id'
  validates :followed_id, presence: true

  def sends_user_follow_notification
    UserFollowNotificationWorker.perform_async(self.user_id, self.followed_id)
  end

  def sends_user_unfollow_notification
    UserUnfollowNotificationWorker.perform_async(self.user_id, self.followed_id)
  end
end
