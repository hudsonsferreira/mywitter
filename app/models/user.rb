class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100#" }, :default_url => "missing_:style.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  has_many :posts, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :relationships, dependent: :destroy
  has_many :followings, through: :relationships
  has_many :followers_relationship, class_name: 'Relationship', foreign_key: 'followed_id'
  has_many :followers, through: :followers_relationship, source: :user

  def following?(user)
    self.relationships.pluck(:followed_id).include?(user.id)
  end

  def relationship(user)
    Relationship.where(user_id: self, followed_id: user).pluck(:id).first
  end
end
