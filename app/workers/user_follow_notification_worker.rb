class UserFollowNotificationWorker
  include Sidekiq::Worker

  def perform(user_id, followed_id)
    user, followed = User.find(user_id), User.find(followed_id)
    user.notifications.create(content: "#{user.email} seguiu você.",
                              checked: false,
                              followed_id: followed.id)
  end
end
