class UserUnfollowNotificationWorker
  include Sidekiq::Worker

  def perform(user_id, unfollowed_id)
    user, unfollowed = User.find(user_id), User.find(unfollowed_id)
    user.notifications.create(content: "#{user.email} deixou de seguir você.",
                              checked: false,
                              followed_id: unfollowed.id)
  end
end
