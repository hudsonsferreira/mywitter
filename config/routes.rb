Rails.application.routes.draw do

  devise_for :users
  resources  :posts,         only: [:index, :new, :create, :destroy, :show]
  resources  :relationships, only: [:create, :destroy]
  resources  :users,         only: [:index, :show]
  resources  :notifications, only: :index

  root to: 'posts#new'

end
