class ChangeRelationships < ActiveRecord::Migration

  change_table :relationships do |t|
    t.rename :follower_id, :followed_id
  end

  add_index :relationships, :user_id
end
