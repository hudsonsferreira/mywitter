class ChangeNotifications < ActiveRecord::Migration
  change_table :notifications do |t|
    t.boolean :checked
    t.integer :followed_id
  end
end
