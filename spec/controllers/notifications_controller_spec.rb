require 'rails_helper'

RSpec.describe NotificationsController, type: :controller do

  describe "GET #index" do

    context "with user" do
      login_user

      before { get :index }

      it { should respond_with(:success) }
      it { should render_template(:index) }

      context "with notifications " do
        let(:current_user)  { controller.current_user }
        let(:user)          { create(:user) }

        it "loads all notifications reversed into @notifications" do
          notification1 = user.notifications.create(attributes_for(:notification, followed_id: current_user.id))
          notification2 = user.notifications.create(attributes_for(:notification, followed_id: current_user.id))
          get :index
          expect(assigns(:notifications)).to_not be_empty
          expect(assigns(:notifications).count).to eq(2)
          expect(assigns(:notifications)).to match_array [notification2, notification1]
        end

        it "updates notification checked attribute to true" do
          notification = user.notifications.create(attributes_for(:notification, followed_id: current_user.id))
          get :index
          expect(notification.checked).to eq(false)
          controller.instance_eval{ update_notifications }
          notification.reload
          expect(notification.checked).to eq(true)
        end

        it "updates notification checked attribute to true and assing it to @notifications" do
          notification = user.notifications.create(attributes_for(:notification, followed_id: current_user.id))
          get :index
          controller.instance_eval{ update_notifications }
          expect(assigns(:notifications).first.checked).to eq(true)
        end
      end

      context "without notifications " do

        it "@notifications should be empty" do
          get :index
          expect(assigns(:notifications)).to be_empty
          expect(assigns(:notifications).count).to eq(0)
        end
      end
    end

    context "without user" do
      it "raises exception" do
        expect{ get :index }.to raise_error(NoMethodError)
      end
    end
  end
end
