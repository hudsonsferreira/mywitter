require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  describe "GET #new" do
    context "with user" do
      login_user

      before { get :new }

      it "should have a current user" do
        expect(controller.current_user).to_not be_nil
      end

      it { should respond_with(:success) }
      it { should render_template(:new) }
      it { should render_template(partial: 'posts/_form') }
      it { should render_template(partial: 'posts/_index') }
      it { expect(assigns(:post)).to be_a_new(Post) }

    end

    context "without user" do
      before { get :new }

      it "should not have a current user" do
        expect(controller.current_user).to be_nil
      end

      it { should respond_with(:success) }
      it { should render_template(:new) }
      it { should_not render_template(partial: 'posts/_form') }
      it { should render_template(partial: 'posts/_index') }
      it { expect(assigns(:post)).to be_nil }
    end

  end

  describe "GET #index" do

    it "loads all of the posts into @posts" do
      post1, post2 = create(:post), create(:post)
      controller.index
      expect(assigns(:posts)).to match_array [post1, post2]
    end

  end

  describe "POST #create" do
    context "with user" do
      login_user

      context "with params" do
        before(:each) do
          request.env["HTTP_REFERER"] = new_post_path
          post :create, post: attributes_for(:post)
        end

        it "creates the post" do
          expect(Post.count).to eq(1)
        end

        it "post shoud includes user" do
          expect(assigns(:post).user).to eq(controller.current_user)
        end

        it { should redirect_to(request.referer) }
      end

      context "without params" do
        let(:action) { post :create, post: attributes_for(:post, content: nil) }

        it "raises exception" do
          expect{ action }.to raise_error(ActionView::Template::Error)
        end
      end
    end

    context "without user" do
      it "raises exception" do
        expect{ post :create, post: attributes_for(:post) }.to raise_error(NoMethodError)
      end
    end
  end

    describe "GET #show" do
      context "with user" do
        login_user

        context "own post" do
          let(:post) { create(:post, user_id: controller.current_user.id) }
          before { get :show, id: post}

          it "assigns the requested post to the @post" do
            expect(assigns(:post)).to eq(post)
          end

          it { should render_template(:show) }
        end

        context "another post owner" do
          let(:user) { create(:user) }
          let(:post) { create(:post, user_id: user.id) }
          before { get :show, id: post}

          it "assigns the requested post to the @post" do
            expect(assigns(:post)).to eq(post)
          end

          it { should render_template(:show) }
        end
      end

      context "without user" do
        let(:user) { create(:user) }
        let(:post) { create(:post, user_id: user.id) }
        before { get :show, id: post}

        it "assigns the requested post to the @post" do
          expect(assigns(:post)).to eq(post)
        end

        it { should render_template(:show) }
      end
    end

  end
