require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do

  describe "POST #create" do
    context "with user" do
      login_user

      let(:current_user) { controller.current_user }
      before { request.env["HTTP_REFERER"] = user_path(controller.current_user) }

      context "with params" do
        let(:user)         { create(:user) }
        let(:relationship) { current_user.relationships.create(followed_id: user.id) }
        before             { post :create, relationship: relationship }

        it "creates the relationship" do
          expect(Relationship.count).to eq(1)
        end

        it { should redirect_to(request.referer) }
      end

      context "without params" do
        let(:relationship) { current_user.relationships.create(followed_id: nil) }
        before             { post :create, relationship: relationship }

        it "doesnt creates the relationship" do
          expect(Relationship.count).to eq(0)
        end

        it { should redirect_to(request.referer) }
      end
    end

    context "without user" do
      it "raises exception" do
        expect{ post :create, relationship: attributes_for(:relationship) }.to raise_error(NoMethodError)
      end
    end

  end

  describe "DELETE #destroy" do

    context "with user" do
      login_user

      let(:relationship) { create(:relationship, user_id: controller.current_user.id) }

      before(:each) do
        request.env["HTTP_REFERER"] = user_path(controller.current_user)
        delete :destroy, id: relationship
      end

      it { should redirect_to(request.referer) }
    end

    context "without user" do

      let(:relationship) { create(:relationship) }

      it "raises exception" do
        expect{ delete :destroy, id: relationship }.to raise_error(NoMethodError)
      end
    end

  end


end
