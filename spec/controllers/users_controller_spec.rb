require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  describe "GET #show" do
    let!(:user) { create(:user) }

    context 'existing user' do
      before { get :show, id: user }

      it 'should finds a user' do
        expect(assigns(:user)).to eq(user)
      end
      it { should respond_with(:success) }
      it { should render_template(:show) }
      it { should render_template(:partial => 'posts/_index') }
    end

    context 'inexistent user' do

      it 'should raises exception' do
        expect{get :show, id: 345}.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

  end

  describe "GET #index" do

    context 'ransack search by email' do

      let!(:user1) { create(:user, email: 'hudson@hudson.com') }
      let!(:user2) { create(:user, email: 'hudson@ferreira.com') }

      context 'finds all users with @' do
        before { get :index, q: { email_cont: '@' } }

        it "loads all users into @users" do
          expect(assigns(:users)).to match_array [user2, user1]
        end

        it { should respond_with(:success) }
        it { should render_template(:index) }
      end

      context 'finds distincts users' do
        before { get :index, q: { email_cont: 'hud' } }

        it "should find the double 'hud' users" do
          expect(assigns(:users)).to match_array [user2, user1]
        end

        it { should respond_with(:success) }
        it { should render_template(:index) }
      end

      context 'finds specific user' do
        before { get :index, q: { email_cont: 'hudson@ferreira.com' } }

        it "should find just one user" do
          expect(assigns(:users).first).to eq(user2)
        end

        it { should respond_with(:success) }
        it { should render_template(:index) }
      end

      context 'doesnt find inexistent user' do
        before(:each) do
          get :index, q: { email_cont: 'foo' }
        end

        it "should be nil" do
          expect(assigns(:users).first).to be_nil
        end

        it "should not find a user" do
          expect(assigns(:users).length).to eq(0)
        end

        it { should respond_with(:success) }
        it { should render_template(:index) }
      end

      context 'finds users without param' do
        before { get :index, q: { email_cont: '' } }

        it "loads all users into @users" do
          expect(assigns(:users)).to match_array [user2, user1]
        end

        it { should respond_with(:success) }
        it { should render_template(:index) }
      end
    end
  end

end
