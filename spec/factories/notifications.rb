FactoryGirl.define do
  factory :notification do
    content     { Faker::Lorem.sentence }
    user_id     { Faker::Number.number(3) }
    checked     false
    followed_id { Faker::Number.number(3) }
  end

end
