FactoryGirl.define do
  factory :post do
    content { Faker::Lorem.sentence }
    user_id { Faker::Number.number(3) }
  end
end
