FactoryGirl.define do
  factory :relationship do
    user_id { Faker::Number.number(3) }
    followed_id { Faker::Number.number(3) }
  end

end
