FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'

    before :create do |user|
      user.avatar_file_name    = "photo.jpg"
      user.avatar_content_type = "image/jpeg"
      user.avatar_file_size    = 2588
      user.avatar_updated_at   = Time.zone.now
    end
  end
end
