require 'rails_helper'

RSpec.feature "Post content process" do

  context "with user" do

    before(:each) do
      login_as( create(:user) )
      visit root_path
    end

    scenario "its possible post content" do
      expect(page).to have_content('Cê tá pensando em que?')
    end

    scenario "it posts content when has content" do
      fill_in 'post_content', :with => 'foo'
      click_button 'Postar'
      expect(page).to have_content('foo')
    end
  end

  context "without user" do

    before { visit root_path }

    scenario "its not possible post content" do
      expect(page).to_not have_content('Cê tá pensando em que?')
    end

    scenario "its possible see timeline " do
      expect(page).to have_content('Timeline')
    end
  end

end
