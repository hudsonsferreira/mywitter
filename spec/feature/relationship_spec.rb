require 'rails_helper'

RSpec.feature "Relationship User" do

  context "with current user" do

    before(:each) do
      login_as( create(:user) )
      user = create(:user)
      visit user_path(user)
    end

    scenario "page should have link 'Seguir'" do
      expect(page).to have_link('Seguir')
    end

    scenario "link should changes from 'Seguir' to 'Deixar de Seguir'" do
      expect(page).to have_link('Seguir')
      click_link('Seguir')
      expect(page).to have_link('Deixar de seguir')
    end

    scenario "link should changes from 'Deixar de Seguir' to 'Seguir'" do
      click_link('Seguir')
      expect(page).to have_link('Deixar de seguir')
      click_link('Deixar de seguir')
      expect(page).to have_link('Seguir')
    end
  end

  context "without current user" do

    before(:each) do
      user = create(:user)
      visit user_path(user)
    end

    scenario "page should not have link 'Seguir'" do
      expect(page).to_not have_link('Seguir')
    end

    scenario "page should not have link 'Deixar de seguir'" do
      expect(page).to_not have_link('Deixar de seguir')
    end
  end
end
