require 'rails_helper'

RSpec.feature "Search form users " do

  context "with user signed in" do

    before(:each) do
      login_as( create(:user) )
      visit root_path
    end

    scenario "its possible to search users" do
      expect(page).to have_button('Buscar')
    end
  end

  context "without user" do

    before { visit root_path }

    scenario "its not possible to search users" do
      expect(page).to_not have_button('Buscar')
    end
  end
end
