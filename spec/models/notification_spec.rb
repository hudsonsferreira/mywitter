require 'rails_helper'

RSpec.describe Notification do
  it { should belong_to(:user) }
  it { should validate_presence_of(:content) }
  it { should validate_presence_of(:followed_id) }

  describe ".about_me" do

    let(:user)     { create(:user) }
    let(:followed) { create(:user) }

    context "with params" do
      before(:each) do
        @notification1 = user.notifications.create(content: 'foo', followed_id: followed.id)
        @notification2 = user.notifications.create(content: 'bar', followed_id: followed.id)
      end

      let(:notifications) { Notification.all.about_me(followed.id) }

      it"should returns all followed user notifications"do
        expect(notifications.count).to eq(2)
      end

      it"should returns all followed user notifications revered"do
        expect(notifications).to match_array [@notification2, @notification1]
      end
    end

    context "with wrong params" do

      let(:notifications) { Notification.all.about_me(355) }

      it "should not find notifications" do
        expect(notifications).to be_empty
      end
    end

    context "without params" do

      it "should raise_error"do
        expect{ Notification.all.about_me() }.to raise_error(ArgumentError)
      end
    end
  end

  describe ".notifications_not_seen_counter" do

    let(:user)     { create(:user) }
    let(:followed) { create(:user) }

    context "with notifications" do
      let(:params) { { content: 'foo', followed_id: followed.id, checked: false } }

      before { user.notifications.create(params) }

      let(:counter) { Notification.notifications_not_seen_counter(followed.id) }

      it "should returns the quantity of not seen notifications" do
        expect(counter).to eq(1)
      end
    end

    context "without notifications" do

      let(:counter) { Notification.notifications_not_seen_counter(followed.id) }

      it "should returns 0" do
        expect(counter).to eq(0)
      end
    end

    context "with seen notifications" do
      let(:params) { { content: 'foo', followed_id: followed.id, checked: true } }

      before { user.notifications.create(params) }

      let(:counter) { Notification.notifications_not_seen_counter(followed.id) }

      it "should returns 0" do
        expect(counter).to eq(0)
      end
    end
  end
end
