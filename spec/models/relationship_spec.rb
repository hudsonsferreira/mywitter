require 'rails_helper'

RSpec.describe Relationship do
  it { should belong_to(:user) }
  it { should belong_to(:following).class_name('User') }
  it { should validate_presence_of(:followed_id) }

  let(:user1) { create(:user) }
  let(:user2) { create(:user) }

  describe "#sends_user_follow_notification" do

    context "runs job" do
      let(:relationship) { user1.relationships.create(followed_id: user2.id) }
      before             { relationship.sends_user_follow_notification }
      let(:counter)      { UserFollowNotificationWorker.jobs.count }

      it "enqueues a user follow notification worker" do
        expect(counter).to eq(1)
      end
    end
  end

  describe "#sends_user_unfollow_notification" do

    context "runs job" do
      let(:relationship) { user1.relationships.create(followed_id: user2.id) }

      before(:each) do
        relationship.sends_user_unfollow_notification
        relationship.destroy
      end

      let(:counter) { UserUnfollowNotificationWorker.jobs.count }

      it "enqueues a user unfollow notification worker" do
        expect(counter).to eq(1)
      end
    end
  end
end
