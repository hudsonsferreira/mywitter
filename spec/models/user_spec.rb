require 'rails_helper'

RSpec.describe User do
  it { should have_many(:posts).dependent(:destroy) }
  it { should have_many(:notifications).dependent(:destroy) }
  it { should have_many(:relationships).dependent(:destroy) }
  it { should have_many(:followings).through(:relationships) }
  it { should have_many(:followers_relationship).class_name('Relationship').with_foreign_key('followed_id') }
  it { should have_many(:followers) }
  it { should have_attached_file(:avatar) }
  it { should validate_attachment_content_type(:avatar).
                allowing('image/png', 'image/gif').
                rejecting('text/plain', 'text/xml') }

  describe "#following?" do
    let(:user1) { create(:user) }
    let(:user2) { create(:user) }

    context "following" do

      before  { user1.followings << user2 }
      subject { user1.following?(user2) }

      it "should return true" do
        expect(subject).to eq(true)
      end
    end

    context "unfollowing" do

      subject { user1.following?(user2) }

      it "should return true" do
        expect(subject).to eq(false)
      end
    end
  end

  describe "#relationship" do
    let(:user1) { create(:user) }
    let(:user2) { create(:user) }

    context "when exists" do

      before { user1.relationships.create(followed_id: user2.id) }
      subject { user1.relationship(user2) }

      it "should return relationship id" do
        expect(subject).to eq(Relationship.last.id)
      end
    end

    context "when not exists" do

      subject { user1.relationship(user2) }

      it "should be nil" do
        expect(subject).to be_nil
      end
    end
  end
end
