require 'rails_helper'

RSpec.describe ApplicationController, :type => :request do

  let(:user) { create(:user) }

  describe "sign_up user with avatar" do

    let(:sign_up_params) { {email: user.email, password: 'password'} }

    before { post_via_redirect user_session_path, user: sign_up_params }

    it 'permits :avatar for :sign_up' do
      expect(user.avatar_file_name).to eq('photo.jpg')
    end
  end

  describe "account_update user with avatar" do

    let(:params) { { email: user.email,
                     password: 'password',
                     password_confirmation: 'password',
                     avatar_file_name: 'another_photo.png',
                     avatar_content_type: 'image/png',
                     avatar_file_size: 1577,
                     avatar_updated_at: Time.zone.now } }

    it 'permits :avatar for :account_update' do
      user.update(params)
      expect(user.avatar_file_name).to eq('another_photo.png')
    end
  end
end
