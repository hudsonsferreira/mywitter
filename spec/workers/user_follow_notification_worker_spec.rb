require 'rails_helper'

RSpec.describe UserFollowNotificationWorker do

  describe ".perform_async" do

    context "runs job" do
      it { should be_processed_in :default }
      it { should be_retryable true }
      it { should_not be_unique }

      let(:user1)   { create(:user) }
      let(:user2)   { create(:user) }
      before        { UserFollowNotificationWorker.perform_async(user1.id, user2.id) }
      let(:counter) { UserFollowNotificationWorker.jobs.count }

      it "enqueues a user follow notification worker" do
        expect(counter).to eq(1)
      end

      it 'enqueues another follllow notification job' do
        expect(UserFollowNotificationWorker).to have_enqueued_job(user1.id, user2.id)
      end
    end
  end

  describe "#perform" do

    context "with params" do
      it { should be_processed_in :default }
      it { should be_retryable true }
      it { should_not be_unique }

      let(:user1)   { create(:user) }
      let(:user2)   { create(:user) }
      let(:worker)  { UserFollowNotificationWorker.new }
      before        { worker.perform(user1.id, user2.id) }

      it "should creates a notification" do
        expect(Notification.last.user_id).to eq(user1.id)
      end

      it "notification should contain 'seguiu' word " do
        expect(Notification.last.content).to include('seguiu')
      end
    end

    context "without params" do
      it { should be_processed_in :default }
      it { should be_retryable true }
      it { should_not be_unique }

      let(:worker) { UserFollowNotificationWorker.new }

      it "should creates a notification" do
        expect{ worker.perform() }.to raise_error(ArgumentError)
      end

    end
  end
end
