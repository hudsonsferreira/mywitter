require 'rails_helper'

RSpec.describe UserUnfollowNotificationWorker do

  describe ".perform_async" do

    context "runs job" do
      it { should be_processed_in :default }
      it { should be_retryable true }
      it { should_not be_unique }

      let(:user1)   { create(:user) }
      let(:user2)   { create(:user) }
      before        { UserUnfollowNotificationWorker.perform_async(user1.id, user2.id) }
      let(:counter) { UserUnfollowNotificationWorker.jobs.count }

      it "enqueues a user unfollow notification worker" do
        expect(counter).to eq(1)
      end

      it 'enqueues another unfollow notification job' do
        expect(UserUnfollowNotificationWorker).to have_enqueued_job(user1.id, user2.id)
      end
    end
  end

  describe "#perform" do

    context "with params" do
      it { should be_processed_in :default }
      it { should be_retryable true }
      it { should_not be_unique }

      let(:user1)   { create(:user) }
      let(:user2)   { create(:user) }
      let(:worker)  { UserUnfollowNotificationWorker.new }
      before        { worker.perform(user1.id, user2.id) }

      it "should creates a notification" do
        expect(Notification.last.user_id).to eq(user1.id)
      end

      it "notification should contain 'deixou de seguir' sentence " do
        expect(Notification.last.content).to include('deixou de seguir')
      end
    end

    context "without params" do
      it { should be_processed_in :default }
      it { should be_retryable true }
      it { should_not be_unique }

      let(:worker) { UserUnfollowNotificationWorker.new }

      it "should creates a notification" do
        expect{ worker.perform() }.to raise_error(ArgumentError)
      end

    end
  end
end
